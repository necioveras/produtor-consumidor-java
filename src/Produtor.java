
class Produtor extends Thread{
	
	private Deposito dep;
	private int intervaloProducao;
	
	private static final int MAX_PRODUCAO = 100;
	
	
	public Produtor(Deposito dep, int valor){
		this.dep = dep;
		this.intervaloProducao = valor; 
	}

	@Override
	public void run() {
		for (int i = 0; i < MAX_PRODUCAO; i++){   //produzir
			dep.colocar();
			//System.out.println("produzindo. Valor atual: " + dep.getNumItens());
			try{
				Thread.sleep(intervaloProducao);
			} catch (InterruptedException exp) {}
		}
		
	}			
	

}
