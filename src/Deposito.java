

public class Deposito {
	
	private int items = 0;
	private final int capacidade = 100;
	
	public int getNumItens(){
		return items;
	}
	
	public synchronized boolean retirar() {
		if (items > 0){
			items=getNumItens() - 1;
			return true;
		}
		return false;
	}
	
	public boolean colocar() {
		items=getNumItens() +1;
		return true;
	}
	
	public static void main(String[] args) throws InterruptedException {
		int count = 0;
		do{			
			
			Deposito dep = new Deposito();
			System.out.println("Iniciando produção ....");
			Produtor p = new Produtor(dep, 50);
			
			System.out.println("Iniciando consumo ....");
			Consumidor c1 = new Consumidor(dep, 150);
			Consumidor c2 = new Consumidor(dep, 100);
			Consumidor c3 = new Consumidor(dep, 150);
			Consumidor c4 = new Consumidor(dep, 100);
			Consumidor c5 = new Consumidor(dep, 150);
			//Startar o produtor
			//...
			p.start();
			c1.start();
			c2.start();
			c3.start();
			c4.start();
			c5.start();
			
			//System.out.println("Valor final da produção: " + dep.getNumItens());
			//Startar o consumidor
			//...
			while ((p.isAlive() || c1.isAlive() || c2.isAlive() || c3.isAlive() || c4.isAlive() || c5.isAlive()));
			System.out.println("Execucao do main da classe Deposito terminada");
			System.out.println("Valor final da produção: " + dep.getNumItens());
			
		} while (count < 1000);
		
	}
}