
class Consumidor extends Thread{
	
	private Deposito dep;
	private int intervaloConsumo;
	
	private static final int MAX_CONSUMO = 20;
	
	
	public Consumidor(Deposito dep, int valor){
		this.dep = dep;
		this.intervaloConsumo = valor; 
	}


	@Override
	public void run() {
		for (int i = 0; i < MAX_CONSUMO; i++){   //produzir			
			try{
				while(!dep.retirar())
					Thread.sleep(intervaloConsumo);
				Thread.sleep(intervaloConsumo);
			} catch (InterruptedException exp) {}
		}

		
	}
	
	
	
	

}
